<?php
require_once 'dbConnect.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    trait regValidation
    {
        public $data = [];

        public function setData(array $data = []):array
        {
            return $this->data = $data;
        }

        public function validation(array $data)
        {
            $this->setData($data);
            $data = [
                'name' => trim(strip_tags(htmlspecialchars($this->data['name']))),
                'email' => trim(strip_tags(htmlspecialchars($this->data['email']))),
                'password' => trim(strip_tags(htmlspecialchars($this->data['password']))),
                'passwordRepeat' => trim(strip_tags(htmlspecialchars($this->data['passwordRepeat'])))
            ];

            $errors = [];

            if (mb_strlen($data['name']) < 2 || mb_strlen($data['name']) > 50 ) {
                $errors[] = 'Имя должно содержать от 2-х до 50-ти символов';
            }
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $errors[] = 'Введите корректный email';
            }
            if (mb_strlen($data['password']) < 2 || mb_strlen($data['password']) > 20) {
                $errors[] = 'Пароль должен содержать от 2-х до 50-ти символов';
            }
            if ($data['password'] != $data['passwordRepeat']) {
                $errors[] = 'Пароли не совпадают';
            }

            if (count($errors) != 0) {
                foreach ($errors as $error) {
                    echo $error . '<br>';
                }

                return $errors;
            } else {
                return $data;
            }
        }
    }


    class regDBQueries extends dbConnect
    {
        use regValidation;

        public $tableName = 'users';

        public function insert($data)
        {
            $data = $this->validation($data);

            $test = [
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => $data['password']
            ];
            if ($test['name'] != '') { // если имя будет не пустое, значит возвратился массив с данными, а не с ошибками

                $sql = "INSERT INTO `{$this->tableName}` (
                    `name`,
                    `email`,
                    `password`
                ) VALUES (
                    :name,
                    :email,
                    :password
                )";
                $stmt = $this->pdo->prepare($sql);

                echo 'data_true';

                return $stmt->execute($test);
            } else {
                return false;
            }
        }
    }

    $obj = new regDBQueries;

    $obj->insert([
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'password' => $_POST['password'],
        'passwordRepeat' => $_POST['passwordRepeat']
    ]);

}
