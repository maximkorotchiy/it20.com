<?php
    session_start();
    require 'registration.php';

    if ($_GET['action'] == 'logout') {
        session_destroy();
        header("Location: /testregistration");
    }
?>

<html>
    <head>
        <title>Reg</title>
        <meta charset="UTF-8">
        <style>
            * {
                margin: 0;
                padding: 0;
            }
            .col {
                width: calc(50% - 2px);
                border: 1px solid #000;
                float: left;
                display: flex;
                justify-content: center;
            }
            .col div .header{
                width: 100%;
                text-align: center;
            }
            .status {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="col">
            <div class="reg">
                <div class="header">
                    <span>РЕГИСТРАЦИЯ</span>
                </div>

                <div>
                    <span class="status regStatus"></span>
                    <form onsubmit="return false;" novalidate id="formreg" action="registration.php" method="POST">
                        <label for="reg_name">Имя: </label><br>
                        <input name="name" id="reg_name" type="text"><br>

                        <label for="reg_email">Email: </label><br>
                        <input name="email" id="reg_email" type="email"><br>

                        <label for="reg_pass">Придумайте пароль: </label><br>
                        <input name="password" id="reg_pass" type="password"><br>

                        <label for="reg_pass_repeat">Повторите пароль: </label><br>
                        <input name="passwordRepeat" id="reg_pass_repeat" type="password"><br><br>

                        <button id="regButton" type="submit">Зарегистрироваться</button>

                    </form>
                </div>
            </div>
        </div>


        <div class="col">
            <div class="reg">
                <div class="header">
                    <span>АВТОРИЗАЦИЯ</span>
                </div>

                <div>
                    <span class="status logStatus">
                        <?php
                            if (!empty($_SESSION['name'])) {
                                echo 'Привет, '.  $_SESSION['name'];
                                ?>
                                    <a href="?action=logout">Выйти</a>
                                <?php
                            }
                        ?>
                    </span>
                    <form  onsubmit="return false;" novalidate id="formlog" action="login.php" method="POST">
                        <label for="log_email">Email: </label><br>
                        <input name="email" id="log_email" type="email"><br>

                        <label for="log_password">Пароль: </label><br>
                        <input name="password" id="log_password" type="password"><br>

                        <button id="logButton" type="submit">Войти</button>
                    </form>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
        <script src="ajax.js"></script>
    </body>
</html>
