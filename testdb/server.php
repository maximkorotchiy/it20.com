<?php
if($_SERVER["REQUEST_METHOD"] == "POST") {


    $host = 'localhost';
    $db = 'testdb';
    $charset = 'utf8';

    $user = 'root';
    $password = '';

    $pdo = '';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $opt = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    );
    $pdo = new PDO($dsn, $user, $password, $opt);


    $arr = [
        'name' => $_POST['name'],
        'surname' => $_POST['surname'],
        'email' => $_POST['email'],
        'message' => $_POST['message']
    ];
//    $arr = [
//        $_POST['name'],
//        $_POST['surname'],
//        $_POST['email'],
//        $_POST['message']
//    ];
//    var_dump($arr);
//

    $sql = "INSERT INTO `messages` (`name`, `surname`, `email`, `message`) 
    VALUES (:name,:surname,:email,:message)";

    $stmt = $pdo->prepare($sql);

//    $stmt->execute($arr);
    if ($_POST['name'] == '') {
        echo 'Пустое имя';
    } else if ($_POST['surname'] == '') {
        echo 'Пустая фамилия';
    }
    else if ($_POST['email'] == '') {
        echo 'Пустой email';
    } else if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        echo 'Введите корректный email';
    } else if ($_POST['message'] == ''){
        echo 'Пустое сообщение';
    } else {
        try {
            $stmt->execute($arr);
            echo 'true';
        } catch(PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }
}

