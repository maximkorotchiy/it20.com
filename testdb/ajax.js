$("button").click(function(){
    var th = $("form");
    var url = th.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: th.serialize(), // serializes the form's elements.
        success: function(data)
        {
            if (data == 'true') {
                alert('Сообщение отправлено');
            } else {
                alert(data);
            }
        }
    });
});