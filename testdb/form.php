<?php
    include 'server.php';
?>
<!DOCTYPE HTML>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <form onsubmit="return false;" action="server.php" method="POST">
            <label for="name">Имя:</label>
            <br>
            <input id="name" name="name" type="text">
            <br>
            <label for="name">Фамилия:</label>
            <br>
            <input name="surname" type="text">
            <br>
            <label for="email">Email:</label>
            <br>
            <input id="email" name="email" type="email">
            <br>
            <label for="msg">Ваше сообщение:</label>
            <br>
            <textarea name="message" id="msg" cols="30" rows="10"></textarea>
            <hr>
            <button type="submit">Отправить</button>
        </form>

        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
        <script src="ajax.js"></script>
    </body>
</html>
