<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>FORM</title>
</head>
<body>
    <span class="status"></span>
    <br>
    <form onsubmit="return false;" method="POST" action="server.php">
        <label for="mail">Email</label>
        <br>
        <input id="mail" type='text' name='mail' />
        <br>

        <label for="phone">Телефон</label>
        <br>
        <input id="phone" type='text' name='telephone' />
        <br>

        <label for="fullName">ФИО</label>
        <br>
        <input id="fullName" type='text' name='fio_user' />
        <br>
        <label for="msg">Сообщение</label>
        <br>
        <textarea id="msg" name='message'></textarea>
        <hr>
        <button type="submit">Отправить</button>
    </form>

    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="ajax.js"></script>

</body>
</html>

