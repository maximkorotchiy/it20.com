<?php
require_once 'db.php';

class query extends dbConnect
{
    public $tableName = 'request';

    public function validation ($dataArray = []) {
        $dataArray = [
            'mail' => trim(strip_tags(htmlspecialchars($dataArray['mail']))),
            'telephone' => trim(strip_tags(htmlspecialchars($dataArray['telephone']))),
            'fio_user' => trim(strip_tags(htmlspecialchars($dataArray['fio_user']))),
            'message' => trim(strip_tags(htmlspecialchars($dataArray['message']))),
        ];


        $errors = [];

        if (!filter_var($dataArray['mail'], FILTER_VALIDATE_EMAIL)) {
            $errors[] = 'Введите корректный email';
        }
        if (strlen($dataArray['telephone']) < 10 || strlen($dataArray['telephone']) > 20 ) {
            $errors[] = 'Телефон должен содержать от 10-х до 20-ти символов';
        }
        if (mb_strlen($dataArray['fio_user']) < 2 || mb_strlen($dataArray['fio_user']) > 100) {
            $errors[] = 'Поле ФИО должно содержвть от 2-х до 100 символов';
        }
        if (mb_strlen($dataArray['message']) < 1) {
            $errors[] = 'Сообщение должно содержать более одного символа';
        }

        if (count($errors) != 0) {
            foreach ($errors as $error) {
                echo $error . '<br>';
            }

            return $errors;
        } else {
            return $dataArray;
        }

    }

    public function insert ($data) {
        $data = $this->validation($data);

        $sql = "INSERT INTO `{$this->tableName}` (
            `mail`, 
            `telephone`, 
            `fio_user`,
            `message`
        ) VALUES (
            :mail, 
            :telephone, 
            :fio_user,
            :message
        )";

        $stmt = $this->pdo->prepare($sql);
        try {
            $stmt->execute($data);
            echo'data_true';
        } catch (PDOException $e) {
//            echo 'Ошибка' . $e->getMessage();
        }
    }
}

$obj = new query;
$obj->insert([
    'mail' => $_POST['mail'],
    'telephone' => $_POST['telephone'],
    'fio_user' => $_POST['fio_user'],
    'message' => $_POST['message']
]);