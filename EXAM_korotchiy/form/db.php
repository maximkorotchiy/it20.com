<?php
class dbConnect
{
    public $host = 'localhost';
    public $db = 'task11';
    public $charset = 'utf8';

    public $user = 'root';
    public $password = '';

    public $pdo = '';

    public function __construct()
    {
        $dsn = "mysql:host=$this->host; dbname=$this->db; charset=$this->charset";
        $opt = [
            PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_ASSOC
        ];

        $this->pdo = new PDO($dsn, $this->user, $this->password, $opt);
    }
}