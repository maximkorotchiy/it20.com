$("button").click(function(){
    var th = $("form");
    var url = th.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: th.serialize(), // serializes the form's elements.
        success: function(data)
        {
            if (data == 'data_true') {
                $(".status").css({'color':'green'});
                $(".status").html('Сообщение отправлено');
            } else {
                $(".status").css({'color':'red'});
                $(".status").html(data);
            }
        }
    });
});