<?php
// task 7 start
function getExtension ($str) {
    $arr = explode(".", $str);

    return end($arr);
}

echo getExtension('myfile.txt');
// task 7 end
echo '<hr>';

// task 8 start
function dateDiff(string $firstDate, string $secondDate) {
    $firstDate = strtotime($firstDate);
    $secondDate = strtotime($secondDate);

    return abs($firstDate - $secondDate) / 60 / 60 / 24;

}

echo dateDiff('04-02-2020', '10-02-2020');
// task 8 end

echo '<hr>';

// task 9 start
function duplicates($input_array) {
    return array_unique($input_array) !== $input_array;
}

echo duplicates(['а','б','в','а']);
// task 9 end

echo '<hr>';

// task 10 start
function insertIntoFileInAlphabet ($str) {
    $array = explode(',', $str);
    asort($array);
    $result = implode($array, ', ');
    $f = fopen('file.txt', 'w+');

    return fwrite($f, $result);
}

echo insertIntoFileInAlphabet('яблоко, молоко, груша, банан, слива');
// task 10 end