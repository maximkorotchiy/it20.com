<?php
//namespace Academy;
class DateFunctionsClass
{
    private static $test = 'лол';
    public $date1 = '';
    private $date2 = '';
    /**
     * установка первой даты
     */
    public function setDate1($date1) {
        return $this->$date1 = $date1;
    }
    /**
     * установка второй даты
     */
    public function setDate2($date2) {
        $this->$date2 = $date2;
    }
    /**
     * дата в формате '2020-02-02' преобразовуется в timestamp
     * @param $date
     * @return int
     */
    public function getDateInTimestamp($date = 'now'): int {
        return strtotime($date);
    }
    /**
     * получение разницы двух дат
     * @param $date1
     * @param $date2
     * @return int
     */
    public function getDifferentDate($date1 = 'now', $date2 = 'now'): int {
        return floor(abs(strtotime($date2) - strtotime($date1)) / (60 * 60 * 24));
    }
    /**
     * определение является ли дата рабочим днем
     * @param $date
     * @return bool
     */
    public function isWorkDate($date = '2008-05-14'): bool {
        return (date("w", strtotime($date))>4) ? false : true;
        //w	Порядковый номер дня недели	от 0 (воскресенье) до 6 (суббота)
    }
    /**
     * функция возвращает словами день недели
     * @param string $date
     * @return string
     */
    public function getNameDayOfWeek($date = ''): string {
        return date("l", strtotime($date));
    }
    public function __construct() {
        echo "myClass init'ed successfuly!!!<br>";
    }
}

$obj = new DateFunctionsClass;
echo $obj->setDate1('2020-20-20');
echo $obj->date1;