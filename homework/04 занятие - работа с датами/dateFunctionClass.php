<?
class DateFunctionsClass{
	private $date1 = '';
	private $date2 = '';

	/**
     * установка первой даты
     */
	public function setDate1($date1)
	{
	    $this->date1 = $date1;
	}

    /**
     * установка второй даты
     */
	public function setDate2($date2)
	{
	    $this->date2 = $date2;
	}

    /**
     * дата в формате '2020-02-02' преобразовуется в timestamp
     * @param $date
     * @return int
     */
    public function getDateInTimestamp($date = "2020-02-02"):int
    {
        return strtotime($date);
    }

    /**
     * получение разницы двух дат
     * @param $date1
     * @param $date2
     * @return int
     */
	public function getDifferentDate($date1 = '', $date2 = ''):int
    {
        $date1 = $this->date1;
        $date2 = $this->date2;

        return abs(strtotime($date1) - strtotime($date2)) / 60 / 60 / 24;
	}

    /**
     * определение является ли дата рабочим днем
     * @param $date
     * @return bool
     */
	public function isWorkDate($date = ''):bool
    {
        $date = $this->getDateInTimestamp();
        $dayDetails = getdate($date);
        switch ($dayDetails['weekday']) {
            case 'Sunday':
            case 'Saturday': $result = false;break;
            default: $result = true;
        }

        return $result;
    }

    /**
     * функция возвращает словами день недели
     * @param string $date
     * @return string
     */
    public function getNameDayOfWeek($date = ''): string
    {
        $date = $this->getDateInTimestamp();
        setlocale(LC_TIME, 'Russian_Russia.1251');
        $dayWeek = iconv(
            'cp1251', 'UTF-8',
            strftime("%A", $date)
        );
        setlocale(LC_TIME, 'eng'); // time to default lang

        return mb_convert_case($dayWeek, MB_CASE_TITLE, 'UTF-8');
    }


}

$obj = new DateFunctionsClass;

$obj->setDate1('2020-03-10');
$obj->setDate2('now');

echo $obj->getDateInTimestamp() . '<br>';
echo $obj->getDifferentDate() . '<br>';
echo $obj->isWorkDate() . '<br>';
echo $obj->getNameDayOfWeek() . '<br>';
