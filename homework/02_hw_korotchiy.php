<?php
// 1st task START --------------------------------------------
function abbreviation ($input_str) {
    $split_input_str = explode(" ", $input_str);
    foreach ($split_input_str as $word) {
        $cut_word = mb_substr($word, 0, 1);
        $result .= mb_strtoupper($cut_word);
    }
    return $result;
}

echo abbreviation("Массачусетский технологический институт");
// 1st task END --------------------------------------------
echo "<hr>";


// 2nd task START --------------------------------------------
function truncate_string ($input_str, $maxsymbol) {
    $length = mb_strlen($input_str);
    $cut = '';
    if ($length > 12) {
        $cut = mb_substr(
            $input_str, 0, $maxsymbol
        ) . '...';
    }
    return $cut;
}

echo truncate_string("Привет, как дела?", 13);
// 2nd task END --------------------------------------------
echo "<hr>";


// 3rd task START --------------------------------------------
function getCountSymbol ($input_str, $symbol) {
    return mb_substr_count($input_str, $symbol);
}

echo getCountSymbol('ананас', 'а');
// 3rd task END --------------------------------------------
echo "<hr>";


// 4th task START --------------------------------------------
?>
<form method="POST">
    <input name="string" type="text">
    <button type="submit">Отправить</button>
</form>
<?php 
    function clearStr() {
        return trim(strip_tags(htmlspecialchars($_POST['string'])));  
    }
    echo clearStr();
// 4th task END --------------------------------------------
echo "<hr>";

// 5th task START --------------------------------------------

function getShortFio($full_name) {
    $split_input_str = explode(" ", $full_name);

    $name = mb_substr($split_input_str[1], 0, 1). '.';
    $patronymic = mb_substr($split_input_str[2], 0, 1). '.';
    
    $result = $split_input_str[0] . ' ' . $name . ' ' . $patronymic;
    return $result;
}

echo getShortFio('Коротчий Максим Александрович');
// 5th task END --------------------------------------------
echo "<hr>";

// 6th task START --------------------------------------------
function fileExtension() {
    $current_file = basename(__FILE__);
    $result = explode(".", $current_file);
    return $result['1'];
}
echo fileExtension();
// 6th task END --------------------------------------------

