<?php 
class firstClass
{
    public static $a = "1";
    public $test = "lol";

    public function test($var1)
    {
        // $var2 = 'hello';
        return $var1;
    }
}

$instance = new firstClass();

echo $instance::$a . '<br>';

// echo($instance->test1);
// echo $instance->test('ара');

// echo $instance->$var2. '<br>';


class secondClass extends firstClass
{
    public $test = "лол2";
    public static $var2 = '1';

    static function b()
    {
        $b = 4;
        return $b;
    }
}

$instance = new secondClass;

echo $instance->test . '<br>';
echo $instance->test('привет') . '<br>';

echo $instance::$var2 . '<br>';
echo secondClass::$var2 . '<br>';

echo secondClass::b();

echo '<hr>';

class testClass1
{
    public static $var1 = 'тест';
    public $var2 = 'var2';
    public $myName;

    function __construct($myName) {
        $this->myName = $myName;
    }
    function getName() {
        return $this->myName;
    }

    public function staticValue() {
        $a = $this->var2;
        return $a;
    }
}

$tc1 = new testClass1("макс");
echo $tc1->getName() . '<hr>';



class testClass2 extends testClass1
{
    public static $tes = 'abc';

    public function var1static() {
        return parent::$var1 . ', ' . static::$tes;
    }
}

// echo testClass1::$var1 . '<br>';



echo $tc1->staticValue() . '<br>';

$tc2 = new testClass2('');
echo $tc2->var1static() . '<br>';

echo '<hr>';

interface CarTemplate
{
    public function getId();
    public function getName();
    public function add();
}

class Audi implements CarTemplate
{
    function getId() {
        return "1-ATHD98";
    }

    function getName() {
        return "Audi";
    }

    function add() {
        
    }
}

$carTemplate = new Audi;

echo $carTemplate->getName();
echo '<hr>';

//class testClass
//{
//    public $myName;
//    function __construct($myName)
//    {
//        $this->myName = $myName;
//    }
//
//    function getInfo(){
//        return $this->myName;
//    }
//}
//
//$tc = new testClass('макс');
//
//echo $tc->getInfo();

echo '<hr>';

class destructableClass
{
    public $myName;
    function __construct($name) {
        echo "деструктор" . '<br>';
        $this->myName = $name;
    }

    function getInfo() {
        return $this->myName . '<br>';
    }

//    function __destruct()
//    {
//        echo "Уничтожение " . $this->myName;
//    }

//    getInfo();
}

$obj = new destructableClass('Макс');

var_dump($obg);
echo '<br>';

echo $obj->getInfo();

echo 'Работает деструктор <br>';

var_dump($obg);
echo "<br>";
echo "<hr>";


trait baseQuery
{
    public $word;
    public function printInfo() {
        return $this->word;
    }
}

class Name
{
    use baseQuery;
    public function getInfo() {

        $this->word = 'Максим';
    }
}

class Surname
{
    use baseQuery;

    public function getInfo() {

        $this->word = 'Коротчий';
    }
}


$name = new Name();
$name->getInfo();

echo $name->printInfo();

$name = new Surname();
$name->getInfo();

echo $name->printInfo();