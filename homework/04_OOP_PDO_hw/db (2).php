<?
class connect
{
    public $host = "localhost";
    public $user = "root";
    public $password = "";

    public $db = "it2.0";
    public $charset = "utf8";

    public $pdo = "";

    public function __construct() {

        $dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
        $opt = array(
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        );
        $this->pdo = new PDO($dsn, $this->user, $this->password, $opt);
    }
}


Class db extends connect{

    public $table_name = 'users';

    /**
     * добавление записи в таблицу
     * @param array $data массив данных для сохранения
     * @return Boolen
     */
    public function insert($data)
    {
        var_dump($data);
        $data['create_at'] = Date('Y-m-d H:i:s');
        $fields = $this->set_fields($data);
        echo $sql = "INSERT INTO `{$this->table_name}` SET ".$fields;
        $stmt = $this->pdo->prepare( $sql );

        return $stmt->execute($data);
    }

    public function update($data)
    {
        $fields = $this->set_fields($data);
        $sql = "UPDATE `{$this->table_name}` SET ".$fields.' WHERE id=:id';

        $stmt = $this->pdo->prepare( $sql );
        return $stmt->execute($data);
    }



    public function deleteAll()
    {
//        $fields = $this->set_fields($data);
        $sql = "DELETE FROM `{$this->table_name}`";

        $stmt = $this->pdo->prepare( $sql );
        return $stmt->execute($data);
    }



    public function set_fields( $items, $delimiter = "," ){
//        print_r($items);
        $str = array();
        if(empty($items)) return "";
        foreach ($items as $key=>$item){
//            echo $key;
            $str[] = "`".$key."`=:".$key;
        }

        return implode($delimiter, $str );
    }

    public function get_count( $where = array() )
    {
//        var_dump($where);
        $sql = "SELECT count(*) FROM {$this->table_name}";
        if( count( $where) > 0 ){
            $fields = $this->set_fields($where, " AND ");
//            var_dump($where);
            $sql .= " WHERE ".$fields;
        }

        $smtp = $this->pdo->prepare($sql);
        $smtp->execute($where);
        $result = $smtp->fetch( PDO::FETCH_NUM );
        var_dump($result);

        return (int)$result[0];
    }


    public function get_all($order = "id asc")
    {
        $sql = "SELECT * FROM `{$this->table_name}` ORDER BY $order";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    /**
     * (new City())->get_one(['id' => 5])
     */
    public function get_one($where = [], $order = "id asc")
    {
        $sql = "SELECT * FROM `{$this->table_name}`";
        if( count( $where) > 0 ){
            $fields = $this->set_fields($where, " AND ");

            $sql .= " WHERE ".$fields;
        }
        $sql .= " ORDER BY $order";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($where);
        $result = $stmt->fetch();

        return $result;
    }

    public function deleteOne($where = [])
    {
        $sql = "DELETE FROM `{$this->table_name}`";
        if( count( $where) > 0 ){
            $fields = $this->set_fields($where, " AND ");
            $sql .= " WHERE ".$fields;
        }

        $stmt = $this->pdo->prepare($sql);

        return $stmt->execute($where);
    }

}

$obj = new db;
//echo $obj->set_fields(
//    ['name']
//);
echo '<br><br>';
$obj->insert(['name' => 'макс']);
echo '<hr>';

echo $obj->update(['id' => 235, 'name' => 'админ']);
echo '<hr>';
echo $obj->get_count(['name' => 'админ']);
echo '<hr>';
echo $obj->deleteOne(['name' => 'макс', 'surname' => 'Коротчий']);

//print_r($obj->get_one(['id' => 3, 'name' => 'админ']));
//echo $obj->deleteAll();
print_r($obj->get_all());