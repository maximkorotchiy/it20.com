<?php

require_once('request.class.php');

$requestClass = new Request();

if( $requestClass->isPost() ){

	$requestClass->required('title');
	$requestClass->required('annotation');
	$requestClass->required('content');
	$requestClass->min('title', 5);
    $requestClass->max('title', 20);
    $requestClass->isEmail('email');
    $requestClass->maxValue('views', 999999);
    $requestClass->minValue('views', 0);

	$errors = $requestClass->getErrors();

    echo json_encode($errors);
}

