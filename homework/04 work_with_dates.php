<?php

// 01 task START
echo '1st task: <br>';

function GetDayWeek($inputDate) {
    $timestamp = strtotime($inputDate);
    setlocale(LC_TIME, 'Russian_Russia.1251');
    $dayWeek = iconv(
        'cp1251', 'UTF-8',
        strftime("%A", $timestamp)
    );
    setlocale(LC_TIME, 'eng'); // time to default lang

    return $dayWeek;
}

echo 'День недели: <b>' . GetDayWeek('1996-04-05') . '</b>';
// 01 task END
echo '<hr>';

// 02 task START
echo '2nd task: <br>';

function GetTomorrowDayWeek() {
    $tomorrowDate = date("Y-m-d", strtotime('next day'));
    $tomorrowDateDetails = getdate(strtotime($tomorrowDate));
    $tomorrowDayWeekNumber = (int) $tomorrowDateDetails['wday'];
    // Day week number to russian day week:
    $tomorrowDayWeek = '';
    switch ($tomorrowDayWeekNumber) {
        case 0:$tomorrowDayWeek = "Воскресенье";break;
        case 1:$tomorrowDayWeek = "Понедельник";break;
        case 2:$tomorrowDayWeek = "Вторник";break;
        case 3:$tomorrowDayWeek = "Среда";break;
        case 4:$tomorrowDayWeek = "Четверг";break;
        case 5:$tomorrowDayWeek = "Пятница";break;
        case 6:$tomorrowDayWeek = "Суббота";break;
    }

    return $tomorrowDayWeek;
}
echo 'Завтра: <b>' . GetTomorrowDayWeek() .  '</b>';

// 02 task END
echo '<hr>';

// 03 task START
echo '3rd task: <br>';

function countDaysBetweenTwoDates($firstDate, $secondDate) {
    $firstDate = strtotime($firstDate);
    $secondDate = strtotime($secondDate);
    $difference =  abs(($firstDate - $secondDate) / 60 / 60 / 24);

    return $difference;
}

echo 'Количество дней между датами: ' . countDaysBetweenTwoDates('01-03-2020', '11-03-2020');
// 03 task END
echo '<hr>';


