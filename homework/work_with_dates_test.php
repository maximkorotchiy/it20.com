<?php
//1996-04-05

//echo DateIntoWord();

$date = '1996-04-05';

$test = strtotime('next Day');
echo date("Y-m-d", $test) . '<br>';

//setlocale(LC_ALL, 'Russian_Russia.1251');
$data = strftime("%A", time());
//echo $data;
echo iconv("cp1251", "UTF-8", $data) . '<hr>';




function DateInWord($inputDate) {
//    $format = date('Y-m-d', $inputDate);
//    return strtotime('now');


    $timestamp = strtotime($inputDate, time());
    $weekDay = getdate ($timestamp);
    print_r($weekDay);
//    $data = date('Y-m-d', strtotime('next day'));
//    $data = date("N", $timestamp);
    echo '<br><br>';
    echo $data;
    echo $wday = $weekDay['wday'];
//    $dayWeek = strftime("%A, %Y-%m-%d", $timestamp) . '<br>';


    switch ($wday) {
        case 1:$wday = "Понедельник";break;
        case 2:$wday = "Вторник";break;
        case 3:$wday = "Среда";break;
        case 4:$wday = "Четверг";break;
        case 5:$wday = "Пятница";break;
        case 6:$wday = "Суббота";break;
        case 7:$wday = "Воскресенье";break;
    }
    return $wday;

}

echo DateInWord('1996-04-05');
//echo time();


echo '<hr>';

// 01 task START
echo '1st task: <br>';
function GetDayWeek($inputDate) {
    $timestamp = strtotime($inputDate, time());
    setlocale(LC_TIME, 'Russian_Russia.1251');
    $dayWeek = iconv(
        'cp1251', 'UTF-8',
        strftime("%A", $timestamp)
    );
    setlocale(LC_TIME, 'eng'); // time to default lang

    return $dayWeek;
}

echo 'День недели: <b>' . GetDayWeek('1996-04-05') . '</b>';

// 01 task END
echo '<hr>';


// 02 task START
echo '2nd task: <br>';

function GetTomorrowDayWeek() {
    $tomorrowDate = date("Y-m-d", strtotime('next day')) . '<br>';
    $tomorrowDateDetails = getdate($tomorrowDate);
    $tomorrowDayWeekNumber = (int) $tomorrowDateDetails['wday'];
    // Day week number to russian day week:
    $tomorrowDayWeek = '';
    switch ($tomorrowDayWeekNumber) {
        case 1:$tomorrowDayWeek = "Понедельник";break;
        case 2:$tomorrowDayWeek = "Вторник";break;
        case 3:$tomorrowDayWeek = "Среда";break;
        case 4:$tomorrowDayWeek = "Четверг";break;
        case 5:$tomorrowDayWeek = "Пятница";break;
        case 6:$tomorrowDayWeek = "Суббота";break;
        case 7:$tomorrowDayWeek = "Воскресенье";break;
    }

    return $tomorrowDayWeek;
}
echo 'Завтра: <b>' . GetTomorrowDayWeek() .  '</b>';

// 02 task END
echo '<hr>';

// 03 task START
echo '3rd task: <br>';

function countDaysBetweenTwoDates($firstDate, $secondDate) {
    $firstDate = strtotime($firstDate);
    $secondDate = strtotime($secondDate);
    $difference =  abs(($firstDate - $secondDate) / 60 / 60 / 24);

    return $difference;
}

echo 'Количество дней между датами: ' . countDaysBetweenTwoDates('01-03-2020', '11-03-2020');
// 03 task END
echo '<hr>';