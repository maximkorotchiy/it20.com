<?php

class dbConnect
{
    public $host = 'localhost';
    public $db = 'exam03test';
    public $charset = 'utf8';

    public $user = 'root';
    public $password = '';

    public $pdo = '';

    public function __construct()
    {
        $dsn = "mysql:host=$this->host; dbname=$this->db; charset=$this->charset";
        $opt = [
            PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_ASSOC
        ];

        $this->pdo = new PDO($dsn, $this->user, $this->password, $opt);
    }
}

class query extends dbConnect
{
    public $tableName = 'messages';

    public function validation ($dataArray = []) {
        $dataArray = [
            'name' => trim(strip_tags(htmlspecialchars($dataArray['name']))),
            'surname' => trim(strip_tags(htmlspecialchars($dataArray['surname']))),
            'message' => trim(strip_tags(htmlspecialchars($dataArray['message'])))
        ];
//        var_dump($dataArray);

        return $dataArray;
    }

    public function insert ($data) {
        $data = $this->validation($data);

        $sql = "INSERT INTO `{$this->tableName}` (
            `name`, `surname`, `message`
        ) VALUES (
            :name, :surname, :message
        )";

        $stmt = $this->pdo->prepare($sql);
        try {
            $stmt->execute($data);
            echo'Сообщение отправлено';
        } catch (PDOException $e) {
            echo 'Ошибка' . $e->getMessage();
        }
    }
}

$obj = new query;
$obj->insert([
    'name' => $_POST['name'],
    'surname' => $_POST['surname'],
    'message' => $_POST['message']
]);