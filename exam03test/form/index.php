<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <title>FORM</title>
    </head>
    <body>
        <form action="server.php" method="POST">
            <label for="name">Имя:</label>
            <br>
            <input name="name" id="name" type="text">
            <br>
            <label for="surname">Фамилия:</label>
            <br>
            <input name="surname" id="surname" type="text">
            <br>
            <label for="message">Сообщение:</label>
            <br>
            <textarea name="message" id="message" cols="30" rows="10"></textarea>

            <button type="submit">Отправить</button>
        </form>
    </body>
</html>