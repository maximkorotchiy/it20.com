$("#regButton").click(function(){
    var th = $("#formreg");
    var url = th.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: th.serialize(), // serializes the form's elements.
        success: function(data)
        {
            if (data == 'data_true') {
                // alert('Вы  успешно зарегистрированы!');
                // $("#status_message").html( "Данные успешно сохранены" );
                $(".regStatus").css({'color':'green'});
                $(".regStatus").html('Вы  успешно зарегистрированы!');
            } else {
                $(".regStatus").css({'color':'red'});
                $(".regStatus").html(data);
            }
        }
    });
});



$("#logButton").click(function(){
    var th = $("#formlog");
    var url = th.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: th.serialize(), // serializes the form's elements.
        success: function(data)
        {
            if (data == 'data_true') {
                setTimeout(function(){
                    location.reload();
                });
            } else {
                $(".logStatus").css({'color':'red'});
                $(".logStatus").html(data);
            }
        }
    });
});