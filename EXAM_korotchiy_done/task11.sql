-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 21 2020 г., 13:42
-- Версия сервера: 5.6.43
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `task11`
--

-- --------------------------------------------------------

--
-- Структура таблицы `request`
--

CREATE TABLE `request` (
  `id` int(11) NOT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fio_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `request`
--

INSERT INTO `request` (`id`, `mail`, `telephone`, `fio_user`, `message`, `file_name`, `create_date`) VALUES
(1, 'mail@mail.com', '2434', 'Коротчий Максим Александрович', 'привет, как дела', NULL, NULL),
(2, 'mail@mail.com', '1243423534434', 'Коротчий Максим Александрович', '24354', NULL, NULL),
(3, 'mail@mail.com', '1243423534434', 'Коротчий Максим Александрович', '24354', NULL, NULL),
(4, 'mail@mail.com', '1243423534434', 'Коротчий Максим Александрович', '34', NULL, NULL),
(5, 'mail@mail.com', '1243423534434', 'Коротчий Максим Александрович', '23', NULL, NULL),
(6, 'mail@mail.com', '1243423534434', 'Коротчий Максим Александрович', '12', NULL, NULL),
(7, 'mail@mail.com', '1243423534434', 'Коротчий Максим Александрович', '12', NULL, NULL),
(8, 'mail@mail.com', '1243423534434', 'Коротчий Максим Александрович', '12', NULL, NULL),
(9, 'mail@mail.com', '1243423534434', 'Коротчий Максим Александрович', '121', NULL, NULL),
(10, 'mail@mail.com', '1243423534434', 'Коротчий Максим Александрович', '121', NULL, NULL),
(11, 'mail@mail.com', '1243423534434', 'Коротчий Максим Александрович', '1212', NULL, NULL),
(12, 'max@max.max', '3453345445', 'Коротчий Максим Александрович', '12342', NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `request`
--
ALTER TABLE `request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
